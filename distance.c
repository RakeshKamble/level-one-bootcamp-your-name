//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

void display(float x1, float y1,float x2, float y2)
{  
   printf("The coordinate points are:\n"); 
   printf("x1=%f, y1=%f\n",x1,y1);
   printf("x2=%f, y2=%f\n",x2,y2);
}


float dist_btw_2p(float *x1,float *y1, float *x2,float *y2)
{
   float dist = sqrt((*x2-*x1)*(*x2-*x1)+(*y2-*y1)*(*y2-*y1));
   return dist;
}


void output(float dist)
{
   printf("The distance between two points are %f\n",dist);
}


int main()
{ 
   float x1,y1,x2,y2;
   printf("Enter the coordinates for x1 and y1:\n");
   scanf("%f %f",&x1,&y1);
   printf("Enter the coordinates for x2 and y2:\n");
   scanf("%f %f",&x2,&y2);
   display(x1,y1,x2,y2);
   float distance = dist_btw_2p(&x1,&y1,&x2,&y2);
   output(distance); 
   return 0;
}
