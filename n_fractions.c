#include <stdio.h>
struct fraction
{
int num,den;
};
typedef struct fraction frac;
frac input()
{
frac s;
printf("enter the values for num and den:\n");
scanf("%d %d",&s.num,&s.den);
return s;
}
void input_n(int n, frac arr[n])
{
for(int i=0; i<n; i++ )
{
arr[i] = input();
}
}
int gcd(int a,int b)
{
int p=1;
for(int i=2; i<=a&&i<=b; i++)
{
if(a%i==0 && b%i==0)
p=i;
}
return p;
}
frac compute(frac c,frac d)
{
frac z;
z.num=c.num*d.den+c.den*d.num;
z.den=c.den*d.den;
int Gcd;
Gcd=gcd(z.num,z.den);
z.num=z.num/Gcd;
z.den=z.den/Gcd;
return z;
}
frac compute_n(int n,frac arr[n])
{
frac sum;
sum.num=0;
sum.den=1;
for(int i=0; i<n; i++ )
{
sum = compute(sum,arr[i]);
}
return sum;
}
void display(int n, frac arr[n],frac sum)
{
for(int i=0;i<n;i++)
{
printf(" %d/%d ",arr[i].num ,arr[i].den);
if(i<n-1)
printf("+");
}
printf(" = %d/%d ",sum.num,sum.den);
}
int main()
{
int n;
printf("enter the value for n:");
scanf("%d",&n);
frac arr[n],sum;
input_n(n,arr);
sum=compute_n( n,arr);
display(n, arr,sum);
return 0;
}
//WAP to find the sum of n fractions.