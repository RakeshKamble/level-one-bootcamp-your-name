#include<stdio.h>
struct fraction
{
int num,den;
};
typedef struct fraction frac;
frac input ()
{
frac s;
printf("enter the num and den:");
scanf("%d %d" ,&s.num,&s.den);
return s;
}
int gcd(int a,int b)
{
int x=1,i;
for(i=2; i<=a && i<=b;i++)
{
if(a%i==0 && b%i==0)
x=i;
}
return x;
}
frac compute(frac c,frac d )
{
frac p;
int Gcd;
p.num = c.num*d.den+c.den*d.num;
p.den=c.den*d.den;
Gcd = gcd(p.num,p.den);
p.num=p.num/Gcd;
p.den=p.den/Gcd;
return p;
}
void display( frac a,frac b,frac sum)
{
printf("%d/%d+%d/%d=%d/%d",a.num,a.den,b.num,b.den,sum.num,sum.den);
}
int main()
{
frac n1,n2,sum;
n1=input();
n2=input();
sum=compute(n1,n2);
display(n1,n2,sum);
return 0;
}