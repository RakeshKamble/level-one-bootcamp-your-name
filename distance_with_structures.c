//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include <math.h>

struct point
{
    float a;
    float b;
};

typedef struct point pnt;
pnt input()
{
    pnt p;
    scanf("%f %f",&p.a,&p.b);
    return p;
}

float dist_btw_2p(pnt p1,pnt p2)
{
    float distance;
    distance=sqrt((p1.a-p2.a)*(p1.a-p2.a)+(p1.b-p2.b)*(p1.b-p2.b));  //Calculate's distance between 2p
    return distance;
}

void output(pnt p1, pnt p2, float dist)
{
    printf("\nSo the distance between (%.2f,%.2f) and (%.2f,%.2f) is %.2f\n)",p1.a,p1.b,p2.a,p2.b,dist);
}

int main(void)
{
    float dist;
    pnt p1,p2;
    printf("Enter first point:\n ");
    p1  = input();
    printf("Enter second point:\n ");
    p2  = input();
    dist  = dist_btw_2p(p1,p2);
    output(p1,p2,dist);
    return 0;
}
